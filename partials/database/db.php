<?php

session_start();
require_once $_SERVER['DOCUMENT_ROOT'] .'/vendor/autoload.php';
require_once('connect.php');

require_once('partials/helpers/validateUser.php');

$errors = array();
$username = '';
$email = '';
$password = '';
$passwordConf = '';

function odd($value) // test
{
    echo "<pre>", print_r($value, true), "</pre>";
    die();
}

function getErrors() {
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
}
//getErrors();

function executeRequete($sql, $data)
{
    global $connect;
    $statement = $connect->prepare($sql);
    $values = array_values($data);
    $types = str_repeat('s', count($values));
    $statement->bind_param($types, ...$values);
    $statement->execute();
    return $statement;
}


function selectAll($table, $conditions = [])
{
    global $connect;
    $sql = "SELECT * FROM $table";
    if (empty($conditions)) {


        $statement = $connect->prepare($sql);
        $statement->execute();
        $records = $statement->get_result()->fetch_all(MYSQLI_ASSOC);
        return $records;
    } else {
        $i = 0;
        foreach ($conditions as $key => $value) {
            if ($i === 0) {
                $sql = $sql . " WHERE $key=?";
            } else {
                $sql = $sql . " AND $key=?";
            }
            $i++;
        }


        $statement = executeRequete($sql, $conditions);
        $records = $statement->get_result()->fetch_all(MYSQLI_ASSOC);
        return $records;

    }
}

function selectOne($table, $conditions)
{
    global $connect;
    $sql = "SELECT * FROM $table";

    $i = 0;
    foreach ($conditions as $key => $value) {
        if ($i === 0) {
            $sql = $sql . " WHERE $key=?";
        } else {
            $sql = $sql . " AND $key=?";
        }
        $i++;
    }

    // equivalent requete SQL: $sql = "SELECT * FROM users WHERE admin=0 AND username='Val' LIMIT 1"
    $sql = $sql . " LIMIT 1";
    $statement = executeRequete($sql, $conditions);
    $records = $statement->get_result()->fetch_assoc();
    return $records;


}


function create($table, $data)
{
    global $connect;
    // $sql = "INSERT INTO users (username, admin, email, password) VALUES (x, x, x, x)"
    // $sql = "INSERT INTO users SET username=?, admin=?, email=?, password=?0"
    $sql = "INSERT INTO users SET ";

    $i = 0;
    foreach ($data as $key => $value) {
        if ($i === 0) {
            $sql = $sql . " $key=?";
        } else {
            $sql = $sql . ", $key=?";
        }
        $i++;
    }

    $statement = executeRequete($sql, $data);
    $id = $statement->insert_id;
    return $id;
}


function update($table, $id, $data)
{
    global $connect;

    // $sql = "UPDATE users SET username=?, admin=?, email=?, password=? WHERE id=?"
    $sql = "UPDATE $table SET ";

    $i = 0;
    foreach ($data as $key => $value) {
        if ($i === 0) {
            $sql = $sql . " $key=?";
        } else {
            $sql = $sql . ", $key=?";
        }
        $i++;
    }

    $sql = $sql . " WHERE id=?";
    $data['id'] = $id;
    $statement = executeRequete($sql, $data);
    return $statement->affected_rows;
}



function delete($table, $id)
{
    global $connect;

    // $sql = "DELETE FROM users WHERE id=?"
    $sql = "DELETE FROM $table WHERE id=?";

    $statement = executeRequete($sql, ['id' => $id]);
    return $statement->affected_rows;
}

