<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/partials/database/db.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/partials/helpers/validatePost.php';

$table = 'posts';

$topics = selectAll('topics');
$posts = selectAll($table);

$errors = array();
$title = "";
$id = "";
$body = "";
$topic = "";
$published = "";

if (isset($_GET['id'])) {
   $post = selectOne($table, ['id' => $_GET['id']]);

    $id = $post['id'];
    $title = $post['title'];
    $body = $post['body'];
    $topic = $post['topic_id'];
    $published = $post['published'];

}

if (isset($_GET['delete_id'])) {
    $count = delete($table, $_GET['delete_id']);
    $_SESSION['message'] = "Le post est bien effacé";
    $_SESSION['type'] = "success";
    header("location: " . BASE_URL . "/admin/posts/index.php");
    exit();
}

if (isset($_GET['published']) && isset($_GET['p_id'])) {
    $published = $_GET['published'];
    $p_id = $_GET['p_id'];
        // ... update post publiés
    $count = update($table, $p_id, ['published' => $published]);
    $_SESSION['message'] = " L'etat de publication du post à changé";
    $_SESSION['type'] = "success";
    header("location: " . BASE_URL . "/admin/posts/index.php");
    exit();
}


if (isset($_POST['add-post'])) {

    $errors = validatePost($_POST);

    if (!empty($_FILES['image']['name'])) {
        $image_name = time() . '_' . $_FILES['image']['name'];
        $destination = ROOT_PATH . "/assets/images/" . $image_name;

        move_uploaded_file($_FILES['image']['tmp_name'], $destination);
    } else {
        array_push($errors, "Une image est necessaire");

        if($result) {
            $_POST['image'] = $image_name;
        } else {
           array_push($errors, "fail to upload image");
        }
    }

    if (count($errors) === 0) {
    unset($_POST['add-post']);
    $_POST['user_id'] = 1;
    $_POST['published'] = isset($_POST['published']) ? 1 : 0;
    $_POST['body'] = htmlentities($_POST['body']);

    $post_id = create($table, $_POST);
    $_SESSION['message'] = "Le post est bien crée";
    $_SESSION['type'] = "success";
    header("location: " . BASE_URL . "/admin/posts/index.php");
    exit();
    } else {
        $title = $_POST['title'];
        $body = $_POST['body'];
        $topic = $_POST['topic_id'];
        $published = isset($_POST['published']) ? 1 : 0;
    }
}

if (isset($_POST['update-post'])) {
    $errors = validatePost($_POST);

    if (!empty($_FILES['image']['name'])) {
        $image_name = time() . '_' . $_FILES['image']['name'];
        $destination = ROOT_PATH . "/assets/images/" . $image_name;

        move_uploaded_file($_FILES['image']['tmp_name'], $destination);
    } else {
        array_push($errors, "Une image est necessaire");
    }
        if($result) {
            $_POST['image'] = $image_name;
        } else {
            array_push($errors, "fail to upload image");
        }

    if (count($errors) === 0) {
        $id = $_POST['id'];
        unset($_POST['update-post'], $_POST['id']);
        $_POST['user_id'] = 1;
        $_POST['published'] = isset($_POST['published']) ? 1 : 0;
        $_POST['body'] = htmlentities($_POST['body']);

        $post_id = update($table, $id, $_POST);
        $_SESSION['message'] = "Le post est bien modifié";
        $_SESSION['type'] = "success";
        header("location: " . BASE_URL . "/admin/posts/index.php");
    } else {
        $title = $_POST['title'];
        $body = $_POST['body'];
        $topic = $_POST['topic_id'];
        $published = isset($_POST['published']) ? 1 : 0;
    }

}
