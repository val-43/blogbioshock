<?php

function validatePost($post)
{

    $errors = array();

    if (empty($post['title'])) {
        array_push($errors, 'Un titre est requis');
    }

    if (empty($post['body'])) {
        array_push($errors, 'Un corps est requis');
    }

    if (empty($post['topic_id'])) {
        array_push($errors, 'Un topic est requis');
    }


    $existingPost = selectOne('posts', ['title' => $post['title']]);
    if ($existingPost) {
        array_push($errors, 'Ce post existe déjà');
    }

    return $errors;
}
