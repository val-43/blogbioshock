<?php

function validateTopic($topic)
{

    $errors = array();

    if (empty($topic['name'])) {
        array_push($errors, 'Un nom de topic est requis');
    }



    $existingTopic = selectOne('topics', ['name' => $topic['name']]);
    if ($existingTopic) {
        array_push($errors, 'Ce topic existe déjà ');
    }

    return $errors;
}

