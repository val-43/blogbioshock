<?php

function validateUser($user)
{

    $errors = array();

    if (empty($user['username'])) {
        array_push($errors, 'Un nom d\'utilisateur est requis');
    }

    if (empty($user['email'])) {
        array_push($errors, 'Un email est requis');
    }

    if (empty($user['password'])) {
        array_push($errors, 'Un mot de passe est requis');
    }

    if ($user['passwordConf'] !== $user['password']) {
        array_push($errors, 'Votre mot de passe n\'est pas identique');
    }

    $existingUser = selectOne('users', ['email' => $user['email']]);
    if ($existingUser) {
        array_push($errors, 'Cette adresse mail est déjà enregistrée');
    }

    return $errors;
}

function validateLogin($user)
{
    $errors = array();
    if (empty($user['username'])) {
        array_push($errors, 'Un nom d\'utilisateur est requis');
    }

    if (empty($user['password'])) {
        array_push($errors, 'Un mot de passe est requis');
    }
    return $errors;
}