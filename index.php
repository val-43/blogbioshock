
<?php require_once("partials/controllers/topics.php"); ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

    <link rel="stylesheet" href="assets/css/style.css">
    <title>BioShock Universe</title>
</head>
<body>
<!-- php header -->
    <?php include("partials/header.php"); ?>
    <?php include("partials/messages.php"); ?>

<div class="page-wrapper">

    <div class="posts-slider">
        <h1 class="slider-title">Trending Posts</h1>
        <i class="fa fa-chevron-right next"></i>
        <i class="fa fa-chevron-left prev"></i>
        <div class="posts-wrapper">
            <div class="post">
                <div class="inner-post">
                    <img src="assets/images/bioshock1.png" alt="" style="height: 200px; width: 100%; border-top-left-radius: 5px; border-top-right-radius: 5px;">
                    <div class="post-info">
                        <h3><a href="single.php">Citation de BioShock ici</a></h3>
                            <div>
                                <i class="fa fa-user-o"></i> Valery
                                &nbsp;
                                <i class="fa fa-calendar"></i> Dec 11, 2020
                            </div>
                    </div>
                </div>
            </div>
            <div class="post">
                <div class="inner-post">
                    <img src="assets/images/bioshock2.jpg" alt="" style="height: 200px; width: 100%; border-top-left-radius: 5px; border-top-right-radius: 5px;">
                    <div class="post-info">
                        <h3><a href="single.php">Ici C'est Rapture</a></h3>
                            <div>
                                <i class="fa fa-user-o"></i> Valery
                                &nbsp;
                                <i class="fa fa-calendar"></i> Dec 11, 2020
                            </div>
                    </div>
                </div>
            </div>
            <div class="post">
                <div class="inner-post">
                    <img src="assets/images/bioshock3.jpg" alt="" style="height: 200px; width: 100%; border-top-left-radius: 5px; border-top-right-radius: 5px;">
                    <div class="post-info">
                        <h3><a href="single.php">A man chooses a slave obeys</a></h3>
                            <div>
                                <i class="fa fa-user-o"></i> Valery
                                &nbsp;
                                <i class="fa fa-calendar"></i> Dec 11, 2020
                            </div>
                    </div>
                </div>
            </div>
            <div class="post">
                <div class="inner-post">
                    <img src="assets/images/bioshock4.jpg" alt="" style="height: 200px; width: 100%; border-top-left-radius: 5px; border-top-right-radius: 5px;">
                    <div class="post-info">
                        <h4><a href="single.php">Booker Devitt</a></h4>
                        <div>
                            <i class="fa fa-user-o"></i> Valery
                            &nbsp;
                            <i class="fa fa-calendar"></i> Dec 11, 2020
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content clearfix">
        <div class="page-content">
            <h1 class="recent-posts-title">Recent Posts</h1>
            <div class="post clearfix">
                <img src="assets/images/bioshock1.png" class="post-image" alt="">
                <div class="post-content">
                    <h2 class="post-title"><a href="#">L'histoire de la saga</a></h2>
                    <div class="post-info">
                        <i class="fa fa-user-o"></i> Valery
                        &nbsp;
                        <i class="fa fa-calendar"></i> Dec 11, 2020
                    </div>
                    <p class="post-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus expedita tempora
                        qui sunt! Ipsum nihil unde obcaecati.
                    </p>
                    <a href="#" class="read-more">Read More</a>
                </div>
            </div>
            <div class="post clearfix">
                <img src="assets/images/bioshock2.png" class="post-image" alt="">
                <div class="post-content">
                    <h2 class="post-title"><a href="#">L'univers du jeu </h2></a>
                    <div class="post-info">
                        <i class="fa fa-user-o"></i> Valery
                        &nbsp;
                        <i class="fa fa-calendar"></i> Dec 11, 2020
                    </div>
                    <p class="post-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus expedita tempora
                        qui sunt! Ipsum nihil unde obcaecati.
                    </p>
                    <a href="#" class="read-more">Read More</a>
                </div>
            </div>
            <div class="post clearfix">
                <img src="assets/images/bioshock3.png" class="post-image" alt="">
                <div class="post-content">
                    <h2 class="post-title"><a href="#">Les references de l'univers</a></h2>
                    <div class="post-info">
                        <i class="fa fa-user-o"></i> Valery
                        &nbsp;
                        <i class="fa fa-calendar"></i> Dec 11, 2020
                    </div>
                    <p class="post-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus expedita tempora
                        qui sunt! Ipsum nihil unde obcaecati.
                    </p>
                    <a href="#" class="read-more">Read More</a>
                </div>
            </div>
        </div>
        <div class="sidebar">
            <!-- Search -->
            <div class="search-div">
                <form action="index.php" method="post">
                    <input type="text" name="search-term" class="text-input" placeholder="Search...">
                </form>
            </div>
            <!-- // Search -->
            <!-- topics -->
            <div class="section topics">
                <h2>Topics</h2>
                <ul>
                    <?php foreach ($topics as $key => $topic): ?>
                        <li><a href="#"><?php echo $topic['name']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- // topics -->
        </div>
    </div>
    <!-- // content -->
</div>


<?php include("partials/footer.php"); ?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="assets/js/scripts.js"></script>
</body>
</html>






