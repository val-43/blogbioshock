<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/partials/controllers/posts.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="stylesheet" href="../../assets/css/admin.css">

    <title>Admin - Edit Topic</title>
</head>

<body>

<?php include("../../partials/adminHeader.php"); ?>

<div class="admin-wrapper clearfix">

    <?php include("../../partials/adminSidebar.php"); ?>

    <div class="admin-content clearfix">
        <div class="button-group">
            <a href="create.php" class="btn btn-sm">Add Post</a>
            <a href="index.php" class="btn btn-sm">Manage Posts</a>
        </div>
        <div class="">
            <h2 style="text-align: center;">Edit Post</h2>

            <form action="edit.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <div class="input-group">
                    <label>Title</label>
                    <input type="text" name="title" value="<?php echo $title ?>" class="text-input">
                </div>
                <div class="input-group">
                    <label>Body</label>
                    <textarea class="text-input" name="body" id="body"><?php echo $body ?></textarea>
                </div>
                <div class="input-group">
                    <div class="input-group">
                        <label>Image</label>
                        <input type="file" name="image" class="text-input" >
                    </div>
                    <label>Topic</label>
                    <select class="text-input" name="topic_id">

                        <option value=""></option>
                        <?php foreach ($topics as $key => $topic): ?>
                            <?php if (!empty($topic_id) && $topic_id == $topic['id'] ): ?>
                                <option selected value="<?php echo $topic['id'] ?>"><?php echo $topic['name'] ?> </option>
                            <?php else: ?>
                                <option value="<?php echo $topic['id'] ?>"><?php echo $topic['name'] ?> </option>
                            <?php endif; ?>

                        <?php endforeach; ?>

                    </select>
                </div>
                <div>
                    <label for="published">
                        <input type="checkbox" name="published" >
                        Publish
                    </label>
                </div>
                <div class="input-group">
                    <?php if (empty($published) && $published == 0): ?>
                        <label>
                            <input type="checkbox" name="publish" /> Publish
                        </label>
                    <?php else: ?>
                        <label>
                            <input type="checkbox" name="publish" checked /> Publish
                        </label>
                    <?php endif; ?>
                </div>
                <div class="input-group">
                    <button type="submit" name="update-post" class="btn" >Update Post</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
<script src="../../assets/js/scripts.js"></script>

</body>

</html>