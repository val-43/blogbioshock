<?php require_once(ROOT_PATH . "/partials/controllers/users.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="stylesheet" href="../../assets/css/admin.css">

    <title>Admin - Manage Users</title>
</head>

<body>

<?php include("../../partials/adminHeader.php"); ?>

<div class="admin-wrapper clearfix">

    <?php include("../../partials/adminSidebar.php"); ?>
    <div class="admin-content clearfix">
        <div class="button-group">
            <a href="create.php" class="btn btn-sm">Add User</a>
            <a href="index.php" class="btn btn-sm">Manage Users</a>
        </div>
        <div class="content">
            <h2 style="text-align: center;">Manage Users</h2>
            <?php include(ROOT_PATH. "/partials/messages.php"); ?>
            <table>
                <thead>
                <th>SN</th>
                <th>Username</th>
                <th colspan="3">Email</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach ($admin_users as $key => $user): ?>
                        <tr class="rec">
                            <td><?php echo $key + 1; ?></td>
                            <td><?php echo $user['username']; ?></td>
                            <td><?php echo $user['email']; ?></td>
                            <td><a href="edit.php" class="edit"> Edit</a></td>
                            <td><a href="index.php?delete_id=<?php echo $user['id']; ?>" class="delete"> Delete</a></td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>

        </div>
    </div>


</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
<script src="../../assets/js/scripts.js"></script>

</body>

</html>