<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/partials/controllers/topics.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="stylesheet" href="../../assets/css/admin.css">

  <title>Admin - Create Topic</title>
</head>

<body>

<?php include("../../partials/adminHeader.php"); ?>

  <div class="admin-wrapper clearfix">

      <?php include("../../partials/adminSidebar.php"); ?>

    <div class="admin-content clearfix">
      <div class="button-group">
        <a href="create.php" class="btn btn-sm">Add Topic</a>
        <a href="index.php" class="btn btn-sm">Manage Topics</a>
      </div>
      <div class="">
        <h2 style="text-align: center;">Create Topic</h2>
          <?php include("partials/helpers/formErrors.php"); ?>
        <form action="create.php" method="post">
          <div class="input-group">
            <label>Name</label>
            <input type="text" name="name" value="<?php echo $name ?>" class="text-input">
          </div>
          <div class="input-group">
            <label>Description</label>
            <textarea class="text-input" name="description" id="body"><?php echo $description ?></textarea>
          </div>
          <div class="input-group">
            <button type="submit" name="save-topic" class="btn" >Save Topic</button>
          </div>
        </form>

      </div>
    </div>


  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
  <script src="../../assets/js/scripts.js"></script>

</body>

</html>