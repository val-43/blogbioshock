<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/partials/controllers/topics.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="stylesheet" href="../../assets/css/admin.css">

    <title>Admin - Manage Topics</title>
</head>

<body>

<?php include("../../partials/adminHeader.php"); ?>

<div class="admin-wrapper clearfix">

    <?php include("../../partials/adminSidebar.php"); ?>
    <div class="admin-content clearfix">
        <div class="button-group">
            <a href="create.php" class="btn btn-sm">Add Topic</a>
            <a href="index.php" class="btn btn-sm">Manage Topics</a>
        </div>
        <div class="">
            <h2 style="text-align: center;">Manage Topic</h2>
            <?php require_once("../../partials/messages.php"); ?>

            <table>
                <thead>
                <th>N</th>
                <th>Name</th>
                <th colspan="2">Action</th>
                </thead>
                <tbody>
                <?php foreach ($topics as $key => $topic): ?>
                <tr class="rec">
                    <td><?php echo $key + 1;?></td>
                    <td><?php echo $topic['name'];?></td>
                    <td><a href="edit.php?id=<?php echo $topic['id']; ?> " class="edit" > Edit </a> </td>
                    <td><a href="index.php?del_id=<?php echo $topic['id']; ?> " class="delete" >Delete</a></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>


</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
<script src="../../assets/js/scripts.js"></script>

</body>

</html>